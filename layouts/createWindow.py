
def createWindow(tk, data, ventana_Top):
    if(data["topLevel"]):
        ventana = tk.Toplevel(ventana_Top)
    else:
        ventana = tk.Tk()
    ventana.title(data["title"])
    ventana.minsize(data["widthmax"], data["heightmax"])
    ventana.resizable(data["resizablewidth"], data["resizableheight"])

    if(data["windowtoCenter"]):
        screen_width = ventana.winfo_screenwidth()
        screen_height = ventana.winfo_screenheight()

    # Calcular el centro de la pantalla
        x_center = (screen_width - data["widthmax"]) // 2
        y_center = (screen_height - data["heightmax"]) // 2

        # Establecer la geometría de la nueva ventana para centrarla
        ventana.geometry(f"+{x_center}+{y_center}")


    return ventana