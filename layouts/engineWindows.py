import tkinter as tk
from tkinter import Menu
import json
from layouts.createWindow import *
from controladors.controladors import * 
from layouts.components.button import Button
from layouts.components.label import Label
from layouts.components.frame import Frame

def createInterface(archivo_json):
    with open(archivo_json, "r",encoding="utf-8") as archivo:
        datos_layout = json.load(archivo)

    ventana = createWindow(tk, datos_layout["ventana"]["data"], None)

    if "options" in datos_layout["ventana"]:
        barra_superior = tk.Menu(ventana)
        ventana.config(menu=barra_superior)
        for options in datos_layout["ventana"]["options"]:
            menu_options = tk.Menu(barra_superior, tearoff=0)
            barra_superior.add_cascade(label=options["label"],menu=menu_options)
            for command in options["command"]:
                accion = command.get("action", None)
                if accion and callable(globals().get(accion["method"])):
                    accion = command.pop("action", None)
                    menu_options_accion_method = accion["method"]
                    menu_options_accion_params = accion["params"]
                    menu_options.add_command(label=command["label"],command=lambda: globals()[menu_options_accion_method](**menu_options_accion_params))
                else:
                    menu_options.add_command(label=command["label"])


    contenedor_principal = Frame({},{"side":"left","fill":"both","expand":1},False,ventana)

    for frame_data in datos_layout["ventana"]["frames"]:
        frame = Frame(frame_data["settings"],frame_data["pack"],frame_data["pack_propagate"],contenedor_principal)
        for widget_data in frame_data["widgets"]:
            tipo_widget = widget_data["type"]
            if tipo_widget == "Label":
                Label(frame,widget_data["settings"],widget_data["pack"], ventana)
            elif tipo_widget == "Button":
                action = widget_data["settings"].pop("action", None)
                if action and callable(globals().get(action["method"])):
                    Button(frame, widget_data["settings"], widget_data["pack"], globals()[action["method"]], action["params"], ventana)
                else:
                    Button(frame,widget_data["settings"],widget_data["pack"],None,None,ventana)

    return ventana
