import os
import tkinter as tk
from layouts.createWindow import * 
from controladors.controladors import *


paso = 1


def abrir_ventana_nueva(ventana):
    global paso
    paso=1

    def nextOrPreviousPaso(value):
        global paso
        paso+=value
        if(paso==1):
            boton1.config(state="disabled")
        if(paso!=1):
            boton1.config(state="active")
        if(paso!=3):
            boton2.config(text="Siguiente")
        if(paso==3):
            boton2.config(text="Finalizar")
        if(paso==4):
            crear_carpeta("TEST")
            nueva_ventana.destroy()
        actualizarTexto()

    def actualizarTexto():
        etiqueta_lateral.config(text="Paso "+ str(paso) +" de 3")

    def crear_carpeta(nombre_carpeta):
        # Obtener la ruta de la carpeta de usuario
        ruta_usuario = os.path.expanduser("~")

        # Crear la ruta completa de la nueva carpeta
        nueva_ruta = os.path.join(ruta_usuario, nombre_carpeta)

        try:
            # Intentar crear la carpeta
            os.mkdir(nueva_ruta)
            print(f'Carpeta "{nombre_carpeta}" creada en la carpeta de usuario.')
        except FileExistsError:
            print(f'La carpeta "{nombre_carpeta}" ya existe en la carpeta de usuario.')

    # Crear una nueva ventana
    data = {
        "topLevel": True,
        "title":"Create Vagrantfile", 
        "widthmax":500, "heightmax":300,
        "resizablewidth" : False , "resizableheight" : False,
        "windowtoCenter": True
    }
    nueva_ventana = createWindow(tk, data, ventana)
    nueva_ventana.attributes('-toolwindow', True)
    nueva_ventana.grab_set()


    # Contenido de la nueva ventana
    etiqueta_nueva = tk.Label(nueva_ventana, text="Esta es una nueva ventana")
    etiqueta_nueva.pack(padx=20, pady=20)

    barra_inferior = tk.Frame(nueva_ventana, height=45, bg="blue")
    barra_inferior.pack(side="bottom", fill="x")
    barra_inferior.pack_propagate(False)

    etiqueta_lateral = tk.Label(barra_inferior, text="Paso "+ str(paso) +" de 3")
    etiqueta_lateral.place(relx=0.5, rely=0.5, anchor="center")

    boton1 = tk.Button(barra_inferior, text="Anterior", state="disabled", height=2, width=10, command=lambda: nextOrPreviousPaso(-1))
    boton1.pack(side="left", padx=10)

    boton2 = tk.Button(barra_inferior, text="Siguiente", height=2, width=10, command=lambda: nextOrPreviousPaso(1))
    boton2.pack(side="right", padx=10)