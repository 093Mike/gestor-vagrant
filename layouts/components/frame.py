import tkinter as tk

class Frame(tk.Frame):
    action = None
    actionparams = None
    ventana = None

    def __init__(self,settings_widget,pack_widget,pack_propagate,ventana,*args, **kwargs):
        self.ventana = ventana
        super().__init__(ventana, *args, **kwargs)
        self.configure(**settings_widget)
        self.pack(**pack_widget)
        self.pack_propagate(pack_propagate)
