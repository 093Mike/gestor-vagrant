import tkinter as tk

class Button(tk.Button):

    action = None
    actionparams = None
    ventana = None

    def __init__(self, frame, settings_widget,pack_widget,action, actionparams, ventana, *args, **kwargs):
        if(not(action == None)):
            self.action = action
            self.actionparams = actionparams
        self.ventana = ventana
        super().__init__(frame, text='', command=self.on_click, *args, **kwargs)        
        self.configure(**settings_widget)
        self.pack(**pack_widget)


    def on_click(self, *args):
        if(not(self.action == None)):
            self.action(self.ventana,**self.actionparams)
