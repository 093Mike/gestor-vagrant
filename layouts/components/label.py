import tkinter as tk

class Label(tk.Label):
    action = None
    actionparams = None
    ventana = None

    def __init__(self,frame,settings_widget,pack_widget,ventana,*args, **kwargs):
        self.ventana = ventana
        super().__init__(frame, text='', *args, **kwargs)
        self.configure(**settings_widget)
        self.pack(**pack_widget)