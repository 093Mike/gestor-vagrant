import tkinter as tk
from tkinter import filedialog

def seleccionar_archivos():
    archivos_seleccionados = filedialog.askopenfilenames(title="Seleccionar archivos", filetypes=[("Archivos de texto", "*.txt")])
    
    # Limpiar la lista actual de widgets
    for widget in lista_archivos:
        widget["frame"].destroy()

    # Crear nuevos widgets para cada archivo seleccionado
    for archivo in archivos_seleccionados:
        frame = tk.Frame(frame_lista_archivos)
        frame.pack(fill=tk.X)

        label = tk.Label(frame, text=archivo)
        label.pack(side=tk.LEFT)

        # Puedes asociar más información aquí, como una ID o cualquier otro dato necesario
        lista_archivos.append({"frame": frame, "archivo": archivo})

# Crear la ventana principal
root = tk.Tk()
root.title("Lista de Archivos")

# Botón para seleccionar archivos
boton_seleccionar = tk.Button(root, text="Seleccionar Archivos", command=seleccionar_archivos)
boton_seleccionar.pack(pady=10)

# Frame para la lista de archivos
frame_lista_archivos = tk.Frame(root)
frame_lista_archivos.pack()

# Lista para almacenar información sobre los archivos seleccionados
lista_archivos = []

# Iniciar el bucle principal de la aplicación
root.mainloop()
