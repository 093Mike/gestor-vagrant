import os
from layouts.createMachineButton import *

def salir():
    exit()
    
def imprimir(ventana, text):
    print(text)


def crear_carpeta(nombre_carpeta):
    # Obtener la ruta de la carpeta de usuario
    ruta_usuario = os.path.expanduser("~")

    # Crear la ruta completa de la nueva carpeta
    nueva_ruta = os.path.join(ruta_usuario, nombre_carpeta)

    try:
        # Intentar crear la carpeta
        os.mkdir(nueva_ruta)
        print(f'Carpeta "{nombre_carpeta}" creada en la carpeta de usuario.')
    except FileExistsError:
        print(f'La carpeta "{nombre_carpeta}" ya existe en la carpeta de usuario.')

# Nombre de la carpeta que deseas crear

# Llamar a la función para crear la carpeta
